#  HTML5 Reset Wordpress Theme (Scotty D. Version)

## v2.0

### Summary:

This is the basic HTML5 Reset theme from [digwp.com](https://digwp.com/2010/02/blank-wordpress-theme/), but I've made some adjustments. I've really come to like this blank theme but there were some updates that I needed to make so I could use it in my every day WordPress theme builds.

Check the change log and commits for updates.

Thanks!