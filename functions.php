<?php

if (!defined('ABSPATH')) exit;


// define max content width
function shapeSpace_content_width()
{

  $GLOBALS['content_width'] = 960;
}
add_action('after_setup_theme', 'shapeSpace_content_width', 0);



// set up theme support
function shapeSpace_setup()
{

  add_theme_support('post-thumbnails');
  add_theme_support('automatic-feed-links');
  add_theme_support('title-tag');
}
add_action('after_setup_theme', 'shapeSpace_setup');

// Enqueue Scripts and Styles
function load_scripts_and_styles()
{
  global $wp_styles;
  // Load jQuery
  if (!is_admin()) {
    wp_deregister_script('jquery');
    wp_register_script('jquery', ("//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"), '', true);


    // register our styles. Might need to change to allow for minified files.
    wp_register_script('custom-js', get_stylesheet_directory_uri() . '/js/scripts.js', array('jquery'), filemtime(get_template_directory() . '/js/scripts.js'), true);

    // register main stylesheet
    wp_enqueue_style('main-styles', get_template_directory_uri() . '/css/style.min.css', array(), filemtime(get_template_directory() . '/css/style.min.css'), false);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
      wp_enqueue_script('comment-reply');
    }

    // enqueue styles and scripts
    wp_enqueue_style('main-styles');
    wp_enqueue_script('jquery');
    wp_enqueue_script('custom-js');
  }
}
add_action('init', 'load_scripts_and_styles');


// register widgets
function shapeSpace_widgets_init()
{

  $widget_args_1 = array(

    'name'          => __('Widgets Sidebar', 'blank-theme'),
    'id'            => 'widgets_sidebar',
    'class'         => '',
    'description'   => __('Widgets added here are displayed in the sidebar', 'blank-theme'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>'

  );

  register_sidebar($widget_args_1);
}
add_action('widgets_init', 'shapeSpace_widgets_init');


// Gutenberg Full Width
add_action('admin_head', 'editor_full_width_gutenberg');

function editor_full_width_gutenberg()
{
  echo '<style>
    body.gutenberg-editor-page .editor-post-title__block, body.gutenberg-editor-page .editor-default-block-appender, body.gutenberg-editor-page .editor-block-list__block {
		max-width: none !important;
	}
    .block-editor__container .wp-block {
        max-width: none !important;
    }
  </style>';
}
