# Changelog

## 3.25.20 - v2.0
Ok, so I didn't realize that the masters at [digwp.com](https://digwp.com/) pushed a large rewrite of their blank theme, which is what this is mainly based from.
So, I've updated to reflect those updates, but kept some of my other elements that I've come to use as well. So more of a "merge" I guess? Anyways, it untested as of now, but I'll push fixes when warranted.